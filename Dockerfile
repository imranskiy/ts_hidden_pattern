FROM mambaorg/micromamba

WORKDIR /app

COPY . ./

RUN micromamba create -n tspattern python=3.11 jupyter -c conda-forge -y


ENTRYPOINT [ "micromamba", "run", "-n", "tspattern", "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root"]
