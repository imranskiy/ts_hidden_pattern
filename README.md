## Building the Docker Image
To build the Docker image, execute the following command in the root directory of the project

docker build -t tspattern .


## Running the Docker Container
This command forwards port 8888 from the container to port 8888 on your host, allowing you to open the Jupyter Notebook in your browser.

docker run -p 8888:8888 tspattern
