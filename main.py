import os


def main(input: str) -> int:
    """_summary_

    Args:
        input (str): _description_

    Returns:
        int: _description_
    """
    print(input)
    return 0


if __name__ == "__main__":
    print(os.listdir())
    print(main("input test"))
