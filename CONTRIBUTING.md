# Contributing to ts_hidden_pattern

///

## Using Linters

We use linters to maintain code quality and consistency throughout the project.
Before committing your changes, please make sure to run the linters to check for any issues in your code.

### Python (black)

To lint Python code, we use [black](https://github.com/psf/black) for code formatting and [flake8](https://flake8.pycqa.org/en/latest/) for code style checking.

Before committing your changes, run the following commands to lint your Python code:

black .
flake8 .
